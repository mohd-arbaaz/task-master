<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    /**
     * RELATIONSHIP METHODS
     */
    public function members()
    {
        return $this->hasMany(User::class);
    }
    public function leader()
    {
        return $this->hasOne(User::class, 'id', 'leader_id');
    }
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
}
