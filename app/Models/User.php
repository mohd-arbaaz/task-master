<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * ACCESSORS
     */

    public function getAvatarAttribute() {
        $size = 45;
        $name = $this->name;
        $length = 2;
        return "https://ui-avatars.com/api/?name={$name}&rounded=true&size={$size}&length={$length}&background=random";
    }

    /**
     * RELATIONSHIP METHODS
     */

    public function team()
    {
        if($this->role == 'leader'){
            return $this->hasOne(Team::class, 'leader_id');
        }
        else if($this->role == 'member'){
            return $this->belongsTo(Team::class);
        }

    }

    public function tasks()
    {
        if($this->role == 'leader') {
            return $this->hasMany(Task::class, 'leader_id');
        }
        else if($this->role == 'member') {
            return $this->hasMany(Task::class, 'member_id');
        }
    }
}
