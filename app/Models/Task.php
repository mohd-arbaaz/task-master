<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Task extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Relationship Methods
     */

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function leader()
    {
        return $this->belongsTo(User::class, 'leader_id');
    }

    public function member()
    {
        return $this->belongsTo(User::class, 'member_id');
    }

    /**
     * ACCESSORS
     */

    public function getTextColorAttribute()
    {
        if($this->status === 'assigned'){
            return "text-info";
        } else if($this->status === 'unresolved') {
            return "text-danger";
        } else if($this->status === 'resolved') {
            return "text-success";
        }
    }

    public function getHasAttachmentAttribute()
    {
        if($this->attachment === null){
            return false;
        }
        return true;
    }

    /**
     * HELPER FUNCTIONS
     */

    public function deleteAttachment(){
        Storage::delete($this->attachment);
    }
}
