<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::where('leader_id', Auth::user()->id)->get();
        return \view('leader.tasks', \compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $members = Auth::user()->team->members;
        return view('leader.create', \compact(['members']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'member_id' => 'required'
        ]);
        $attachment = null;
        if($request->hasFile('attachment')) {
            $attachment = $request->file('attachment')->store('tasks');
        }

        Task::create([
            'title' => $request->title,
            'body' => $request->body,
            'member_id' => $request->member_id,
            'leader_id' => Auth::user()->id,
            'team_id' => Auth::user()->team->id,
            'attachment' => $attachment
        ]);

        session()->flash('success', 'Task Created Successfully');
        return \redirect(\route('leader.tasks.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        return \view('leader.show', \compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        $members = $task->team->members;
        return view('leader.edit', \compact([
            'task',
            'members'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $data = $request->only(['title', 'body', 'member_id']);
        if($request->hasFile('attachment')){
            $attachment = $request->attachment->store('tasks');
            $task->deleteAttachment();
            $data['attachment'] = $attachment;
        }
        $task->update($data);

        session()->flash('success', 'Task Updated Successfully');
        return \redirect(\route('leader.tasks.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->deleteAttachment();
        $task->delete();
        session()->flash('success', 'Task Deleted Successfully!');
        return redirect(route('leader.tasks.index'));
    }

    public function download(Task $task)
    {
        return Storage::download($task->attachment);
    }

    public function memberTasks()
    {
        $tasks = Auth::user()->tasks;
        return view('member.tasks', \compact('tasks'));
    }

    public function memberShow(Task $task)
    {
        return view('member.show', \compact('task'));
    }

    public function memberAssignedTasks()
    {
        $tasks = Task::where([
            'member_id' => Auth::user()->id,
            'status' => 'assigned'
        ])->get();
        return view('member.assignedTasks', \compact('tasks'));
    }
    public function memberUnresolvedTasks()
    {
        $tasks = Task::where([
            'member_id' => Auth::user()->id,
            'status' => 'unresolved'
        ])->get();
        return view('member.unresolvedTasks', \compact('tasks'));
    }
    public function memberResolvedTasks()
    {
        $tasks = Task::where([
            'member_id' => Auth::user()->id,
            'status' => 'resolved'
        ])->get();
        return view('member.resolvedTasks', \compact('tasks'));
    }
    public function markAsResolved(Task $task)
    {
        $task->status = 'resolved';
        $task->update();

        \session()->flash('success', 'Task Marked as Resolved');
        return \redirect(route('member.tasks.resolved'));
    }

    public function markAsUnresolved(Task $task)
    {
        $task->status = 'unresolved';
        $task->update();

        \session()->flash('success', 'Task Marked as Unresolved');
        return \redirect(route('member.tasks.unresolved'));
    }
}
