<?php

namespace Database\Seeders;

use App\Models\Task;
use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(rand(10, 15))->create();
        foreach(User::whereNull('role')->inRandomOrder()->get() as $user){
            $user->role = 'leader';
            $user->update();
            Team::factory()->create(['leader_id' => $user->id])
                ->each(function($team){
                    User::factory(rand(5,10))->create(['role' => 'member', 'team_id' => $team->id])->each(function($member){
                            Task::factory(rand(5,10))->create([
                                'team_id' => $member->team->id,
                                'member_id' => $member->id,
                                'leader_id' => $member->team->leader->id
                            ]);
                        }
                    );
                }
            );
        }
    }
}
