<?php

use App\Http\Controllers\TasksController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth'])->group(function(){

    Route::middleware(['role:leader'])->group(function(){
        Route::name('leader.')->group(function(){
            Route::get('/leader', [UserController::class, 'leader'])->name('index');
            Route::get('/leader/members', [UserController::class, 'leaderMembers'])->name('members');
            Route::resource('/leader/tasks', TasksController::class);
        });
    });

    Route::middleware(['role:member'])->group(function(){
        Route::name('member.')->group(function(){
            Route::get('/member', [UserController::class, 'member'])->name('index');
            Route::get('/member/task/{task}', [TasksController::class, 'memberShow'])->name('tasks.show');
            Route::get('/member/tasks', [TasksController::class, 'memberTasks'])->name('tasks');
            Route::get('/member/assigned-tasks', [TasksController::class, 'memberAssignedTasks'])->name('tasks.assigned');
            Route::get('/member/resolved-tasks', [TasksController::class, 'memberResolvedTasks'])->name('tasks.resolved');
            Route::get('/member/unresolved-tasks', [TasksController::class, 'memberUnresolvedTasks'])->name('tasks.unresolved');
            Route::put('/member/tasks/{task}/resolved', [TasksController::class, 'markAsResolved']);
            Route::put('/member/tasks/{task}/unresolved', [TasksController::class, 'markAsUnresolved']);
        });
    });

    Route::get('/download/{task}', [TasksController::class, 'download'])->name('task.download');
});

