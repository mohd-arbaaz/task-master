@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card shadow-sm">
                <div class="card-header">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{ $task->member->avatar }}" alt="" class="img-fluid">
                            </div>
                            <div class="col-md-8">
                                <h2>{{ $task->title }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    {!! $task->body !!}
                    <p><span class="text-bold"></p>
                    <p><span class="font-weight-bold">Status: </span> <span class="{{ $task->textColor }}">{{ Str::ucfirst($task->status) }}</span></p>
                    @if ($task->hasAttachment)
                            <p>
                                <a href="{{ route('task.download', $task->id) }}" class="btn btn-outline-secondary">
                                    Download Attachment <i class="fa fa-download"></i>
                                </a>
                            </p>
                    @endif
                    <p class="text-right"><span class="font-weight-bold">Assigned By:</span> {{ $task->leader->name }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card shadow">
                <div class="card-header">
                    <h5 class="card-title">Task action</h5>
                </div>
                <div class="card-body">
                    @if ($task->status === 'assigned')
                        <p>
                            <a href="#" class="btn btn-block btn-outline-success btn-sm" onclick="displayResolvedForm({{$task}})"
                            data-toggle="modal" data-target="#resolvedModal">
                            Mark as Resolved <i class="fa fa-check"></i></a>
                        </p>
                        <p>
                            <a href="#" class="btn btn-block btn-outline-danger btn-sm"
                            onclick="displayUnresolvedForm({{$task}})"
                            data-toggle="modal" data-target="#unresolvedModal">
                                Mark as Unresolved <i class="fa fa-close"></i></a>
                        </p>
                    @else
                        <p class="{{ $task->textColor }} font-weight-bold">Task {{ $task->status }}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="resolvedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Mark as Resolved</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="resolvedForm">
                    @csrf
                    @method('PUT');
                    <div class="modal-body">
                        <p>Are you sure you want to mark as Resolved?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-outline-success">Mark as Resolved</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="unresolvedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Mark as Unresolved</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="unresolvedForm">
                    @csrf
                    @method('PUT');
                    <div class="modal-body">
                        <p>Are you sure you want to mark as unresolved?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-outline-danger">Mark as Unresolved</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        function displayResolvedForm($task){
            var url = '/member/tasks/' + $task.id + '/resolved';
            $('#resolvedForm').attr('action', url);
        }
        function displayUnresolvedForm($task){
            var url = '/member/tasks/' + $task.id + '/unresolved';
            $('#unresolvedForm').attr('action', url);
        }
    </script>
@endsection
