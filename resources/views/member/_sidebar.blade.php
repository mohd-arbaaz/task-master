@section('sidebar')
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('member.tasks') }}">All Tasks</a></li>
        <li class="list-group-item"><a href="{{ route('member.tasks.assigned') }}">Assigned Tasks</a></li>
        <li class="list-group-item"><a href="{{ route('member.tasks.resolved') }}">Resolved Tasks</a></li>
        <li class="list-group-item"><a href="{{ route('member.tasks.unresolved') }}">Unresolved Tasks</a></li>
    </ul>
@endsection
