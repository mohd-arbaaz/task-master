@extends('layouts.app')

@include('member._sidebar')

@section('main-content')
    <div class="row">
        @foreach ($tasks as $task)
            <div class="col-md-4 mb-4">
                <div class="card h-100 shadow">
                    <div class="card-body">
                        <div class="row h-25">
                            <div class="col-md-3 align-self-center p-2">
                                <img src="{{ $task->leader->avatar }}" alt="" class="w-100 rounded align-self-center my-auto">
                            </div>
                            <div class="col-md-9 mb-1">
                                <h6 class="card-title align-self-center mb-0">{{ $task->title }}</h6>
                            </div>
                        </div>
                        <div class="d-flex align-items-end h-75">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="{{ $task->textColor }} mb-0">{{ Str::ucfirst($task->status) }}</p>
                                    <p class="mb-2"><span class="font-weight-bold">Assigned by:</span> {{ $task->leader->name }}</p>
                                    <a href="{{ route('member.tasks.show', $task->id) }}" class="btn btn-outline-primary btn-sm ml-auto"><i class="fa fa-eye"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
