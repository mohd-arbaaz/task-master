@extends('layouts.app')

@include('leader._sidebar')

@section('main-content')
    <div class="row">
        @foreach ($members as $member)
            <div class="col-md-4 mb-4">
                <div class="card h-100 shadow">
                    <div class="card-body">
                       <div class="media">
                           <img src="{{ $member->avatar }}" alt="User Profile" class="mr-3">
                           <div class="media-body">
                                <h5 class="card-title">{{ $member->name }}</h5>
                                <p class="card-text mb-0">Tasks Assigned: {{ $member->tasks()->where('status', 'assigned')->count() }}</p>
                                <p class="card-text mb-0">Tasks Resolved: {{ $member->tasks()->where('status', 'resolved')->count() }}</p>
                                <p class="card-text">Tasks Unresolved: {{ $member->tasks()->where('status', 'unresolved')->count() }}</p>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
