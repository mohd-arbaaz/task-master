@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{ $task->member->avatar }}" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-8">
                            <h2>{{ $task->title }}</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                {!! $task->body !!}
                <p><span class="text-bold"></p>
                <p><span class="font-weight-bold">Status: </span> <span class="{{ $task->textColor }}">{{ Str::ucfirst($task->status) }}</span></p>
                @if ($task->hasAttachment)
                        <p>
                            <a href="{{ route('task.download', $task->id) }}" class="btn btn-outline-secondary">
                                Download Attachment <i class="fa fa-download"></i>
                            </a>
                        </p>
                @endif
                <p class="text-right"><span class="font-weight-bold">Assigned To:</span> {{ $task->member->name }}</p>
            </div>
        </div>
    </div>
@endsection
