@extends('layouts.app')

@include('leader._sidebar')

@section('main-content')
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex justify-content-end mb-3">
                <a href="{{ route('leader.tasks.create') }}" class="btn btn-outline-primary">New Task <i class="fa fa-plus"></i></a>
            </div>
        </div>
        @foreach ($tasks as $task)
            <div class="col-md-4 mb-4">
                <div class="card h-100 shadow">
                    <div class="card-body">
                        <div class="row h-25">
                            <div class="col-md-3 align-self-center p-2">
                                <img src="{{ $task->member->avatar }}" alt="" class="w-100 rounded align-self-center my-auto">
                            </div>
                            <div class="col-md-9 mb-1">
                                <h6 class="card-title align-self-center mb-0">{{ $task->title }}</h6>
                            </div>
                        </div>
                        <div class="d-flex align-items-end h-75">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="{{ $task->textColor }} mb-0">{{ Str::ucfirst($task->status) }}</p>
                                    <p class="mb-2"><span class="font-weight-bold">Assigned to:</span> {{ $task->member->name }}</p>
                                    <a href="{{ route('leader.tasks.show', $task->id) }}" class="btn btn-outline-primary btn-sm ml-auto"><i class="fa fa-eye"></i></a>
                                    <a href="{{ route('leader.tasks.edit', $task->id) }}" class="btn btn-outline-secondary btn-sm ml-auto"><i class="fa fa-pencil"></i></a>
                                    <a href="{{ route('leader.tasks.destroy', $task->id) }}"
                                        class="btn btn-outline-danger btn-sm ml-auto" onclick="displayModalForm({{$task}})"
                                        data-toggle="modal" data-target="#deleteModal">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete Task</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="deleteForm">
                    @csrf
                    @method('DELETE');
                    <div class="modal-body">
                        <p>Are you sure you want to delete the task?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline-danger">Delete Task</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        function displayModalForm($task){
            var url = '/leader/tasks/' + $task.id;
            $('#deleteForm').attr('action', url);
        }
    </script>
@endsection
