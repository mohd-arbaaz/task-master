@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Edit Post</div>

        <div class="card-body">
            <form action="{{ route('leader.tasks.update', $task->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" id="title" name="title" placeholder="Enter Title" value = "{{ old('title', $task->title) }}"
                        class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}">
                    @error('title')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">Body</label>
                    <input id="body" type="hidden" name="body" value="{{ old('body', $task->body) }}">
                    <trix-editor input="body" placeholder="Enter body" value="{{ old('body', $task->body) }}"></trix-editor>
                    @error('body')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="member_id">Member</label>
                    <select name="member_id" id="member_id" class="form-control select2">
                        <option value="0" selected disabled>Select Member</option>
                        @foreach ($members as $member)
                            <option value="{{ $member->id }}"
                                @if($member->id == old('member_id', $task->member_id)) selected @endif>{{ $member->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <div class="custom-file">
                        <label for="attachment" class="custom-file-label">Select Attachment</label>
                        <input type="file" name="attachment" id="attachment" class="custom-file-input @error('attachment') is-invalid @enderror">
                        @error('attachment')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-outline-success">Edit Task</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
