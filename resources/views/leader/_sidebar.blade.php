@section('sidebar')
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('leader.members') }}">Team Members</a></li>
        <li class="list-group-item"><a href="{{ route('leader.tasks.index') }}">Tasks</a></li>
    </ul>
@endsection
